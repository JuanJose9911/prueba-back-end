<?php

namespace App\Http\Controllers;

use App\Models\Inventario;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isEmpty;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id_bodega' => 'required',
                'id_producto' => 'required',
                'cantidad' => 'required'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 201);
            }
            $inventario = DB::select(
                'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                [
                    'id_bodega' => $request->id_bodega,
                    'id_producto' => $request->id_producto
                ]
            );
            if ($inventario != []) {
                //si entra aqui el producto ya existe en esa bodega
                //por  tanto se hace un update
                DB::update(
                    'update inventarios set cantidad =cantidad + :cantidad where id_bodega = :id_bodega AND id_producto = :id_producto',
                    [
                        'cantidad' => $request->cantidad,
                        'id_producto' => $request->id_producto
                    ]
                );
                $newInventario = DB::select(
                    'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                    [
                        'id_bodega' => $request->id_bodega,
                        'id_producto' => $request->id_producto
                    ]
                );
                return response()->json([
                    'message' => 'Inventario actualizado correctamente!',
                    'Inventario actualizado' => $newInventario[0],
                    'successfull' => true
                ], 200);
            } else {
                //Si entra aqui el producto no existe en la bodega
                //se hace un insert
                $newInventario = Inventario::create(
                    [
                        'id_bodega' => $request->id_bodega,
                        'id_producto' => $request->id_producto,
                        'cantidad' => $request->cantidad
                    ]
                );
                return response()->json([
                    'message' => 'Inventario creado correctamente!',
                    'bodega' => $newInventario,
                    'successfull' => true
                ], 200);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
