<?php

namespace App\Http\Controllers;

use App\Models\Bodega;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class BodegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $bodegas = DB::table('bodegas')
                            ->select('bodegas.*')
                            ->orderBy('nombre','asc')
                            ->get();
            return response()->json([
                'bodegas' => $bodegas
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * nombre: nombre de la bodega
     *id_responsable :  id del usuario que crea la bodega
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required|string|max:30',
                'id_responsable' => 'required'
            ]);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 201);
            }
            $bodega = Bodega::create(
                [
                    'nombre' => $request->nombre,
                    'id_responsable' => $request->id_responsable
                ]
            );

            return response()->json([
                'message' => 'Bodega registrada correctamente!',
                'bodega' => $bodega,
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function listarProductos()
    {
        try {
            $productos = DB::table('bodegas')
                                ->join('inventarios', 'bodegas.id','inventarios.id_bodega')
                                ->join('productos', 'inventarios.id_producto', 'productos.id')
                                ->select(
                                    'bodegas.id as id-bodega',
                                    'inventarios.id as id-inventario',
                                    'inventarios.cantidad as cant-prod',
                                    'inventarios.id_producto',
                                    'productos.id as id-prod',
                                    'productos.nombre as nombre-prod',
                                    'productos.descripcion as des-prod'
                                )
                                ->get();
            
            return response()->json([
                'message' => 'Bodega registrada correctamente!',
                'productos' => $productos,
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
