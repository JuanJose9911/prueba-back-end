<?php

namespace App\Http\Controllers;

use App\Models\Historial;
use App\Models\Inventario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistorialController extends Controller
{
    /**
     * Display a listing of the resource.
     * id_producto: Recibe el id del producto que se desea trasladar
     * cantidad: recibe la cantidad del producto que se desea trasladar
     * id_bodega_origen: id de la bodega de donde saldra el producto
     * id_bodega_destino: id de la bodega a la que llegara el producto
     *
     * @return \Illuminate\Http\Response
     */
    public function trasladarProducto(Request $request)
    {
        try {
            $inventario = DB::select(
                'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                [
                    'id_bodega' => $request->id_bodega_origen,
                    'id_producto' => $request->id_producto
                ]
            );
            $inventarioDestino = DB::select(
                'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                [
                    'id_bodega' => $request->id_bodega_destino,
                    'id_producto' => $request->id_producto
                ]
            );
            $bodegaDestino = DB::select(
                'select * from bodegas where id = :id_bodega_destino',
                [
                    'id_bodega_destino' => $request->id_bodega_destino
                ]
            );
            if ($inventario != []) {
                if ($request->cantidad > $inventario[0]->cantidad) {
                    return "No hay suficiente cantidad del producto en el inventario";
                } elseif ($bodegaDestino == []) {
                    return "La bodega destino no existe";
                } else {
                    return DB::transaction(function () use ($request,$inventarioDestino) {
                        DB::update(
                            'update inventarios set cantidad =cantidad - :cantidad where id_bodega = :id_bodega AND id_producto = :id_producto',
                            [
                                'cantidad' => $request->cantidad,
                                'id_bodega' => $request->id_bodega_origen,
                                'id_producto' => $request->id_producto
                            ]
                        );
                        $newInventarioOrigen = DB::select(
                            'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                            [
                                'id_bodega' => $request->id_bodega_origen,
                                'id_producto' => $request->id_producto
                            ]
                        );
                        if ($inventarioDestino == []) {
                            $newInventario = Inventario::create(
                                [
                                    'id_bodega' => $request->id_bodega_destino,
                                    'id_producto' => $request->id_producto,
                                    'cantidad' => $request->cantidad
                                ]
                            );
                            return response()->json([
                                'message' => 'Inventario creado correctamente!',
                                'bodega' => $newInventario,
                                'successfull' => true
                            ], 200);
                        }else{
                            DB::update(
                                'update inventarios set cantidad =cantidad + :cantidad where id_bodega = :id_bodega AND id_producto = :id_producto',
                                [
                                    'cantidad' => $request->cantidad,
                                    'id_bodega' => $request->id_bodega_destino,
                                    'id_producto' => $request->id_producto
                                ]
                            );
                            $newInventarioDestino = DB::select(
                                'select * from inventarios where id_bodega = :id_bodega AND id_producto = :id_producto',
                                [
                                    'id_bodega' => $request->id_bodega_destino,
                                    'id_producto' => $request->id_producto
                                ]
                            );
                        }
                        
                        $registroHistorial = Historial::create(
                            [
                                'cantidad' => $request->cantidad,
                                'id_bodega_origen' => $request->id_bodega_origen,
                                'id_bodega_destino' => $request->id_bodega_destino,
                                'id_inventarios' => $newInventarioOrigen[0]->id
                            ]
                        );

                        return [$newInventarioOrigen,$newInventarioDestino,$registroHistorial];
                    });
                }
            } else {
                return "El producto que se desea trasladar o la bodega de origen no existe";
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
