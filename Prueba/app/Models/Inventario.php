<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bodega;
use App\Models\Producto;

class Inventario extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_bodega',
        'cantidad',
        'id_producto'
    ];

    //Inventarios_bodegas
    public function users(){
        return $this-> belongsTo(Bodega::class, 'id_bodega');
    }

    //Inventarios_productos
    public function productos(){
        return $this-> belongsTo(Producto::class, 'id_producto');
    }

    //Inventarios_historial
    public function historial(){
        return $this->hasMany(Historial::class);
    }
}
