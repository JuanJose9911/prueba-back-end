<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Bodega;
use App\Models\Inventario;
use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    use HasFactory;
    protected $table = "historiales";
    protected $fillable = [
        'cantidad',
        'id_bodega_origen',
        'id_bodega_destino',
        'id_inventarios'
    ];

    //Historial_bodega_origen
    public function bodega_origen(){
        return $this-> belongsTo(Bodega::class, 'id_bodega_origen');
    }

    //Historial_bodega_origen
    public function bodega_destino(){
        return $this-> belongsTo(Bodega::class, 'id_bodega_destino');
    }

    //Historial_inventarios
    public function inventarios(){
        return $this-> belongsTo(Inventario::class, 'id_inventario');
    }
}
