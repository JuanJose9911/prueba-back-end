<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Inventario;
use App\Models\Historial;

class Bodega extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'id_responsable'
    ];

    //Bodegas_users
    public function users(){
        return $this-> belongsTo(User::class, 'id_responsable');
    }

    //Bodegas_inventarios
    public function bodegas(){
        return $this->hasMany(Inventario::class);
    }

    //Bodegas_historiales
    public function historial(){
        return $this->hasMany(Historial::class);
    }
}
