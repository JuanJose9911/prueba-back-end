<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Bodega;
use App\Models\Producto;

class InventarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_bodega' => Bodega::factory(),
            'id_producto' => Producto::factory(),
            'cantidad' => $this->faker->randomNumber()
        ];
    }
}
