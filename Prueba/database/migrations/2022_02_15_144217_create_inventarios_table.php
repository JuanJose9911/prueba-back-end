<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventarios', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad');
            $table->tinyInteger('estado')->default(1);
            //Clave foranea bodega
            $table->foreignId('id_bodega')
                    ->nullable()
                    ->constrained('bodegas')
                    ->unique()
                    ->default(1)
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            //Clave foranea productos 
            $table->foreignId('id_producto')
                    ->nullable()
                    ->constrained('productos')
                    ->unique()
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();//deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventarios');
    }
}
