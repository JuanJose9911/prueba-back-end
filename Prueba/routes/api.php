<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    
    //bodegas
    Route::get('listar-bodegas', 'App\Http\Controllers\BodegaController@index');
    Route::post('crear-bodega','App\Http\Controllers\BodegaController@create');
    Route::get('listar-productos','App\Http\Controllers\BodegaController@listarProductos');

    //productos
    Route::post('agregar-producto', 'App\Http\Controllers\ProductoController@create');

    //inventario
    Route::post('inventario','App\Http\Controllers\InventarioController@store');

    //historial
    Route::post('mover-producto','App\Http\Controllers\HistorialController@trasladarProducto');
});
